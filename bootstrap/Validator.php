<?php


namespace Bootstrap;



use Exceptions\Validator\ValidationException;

class Validator
{
    /**
     * @param array $params
     * @param array $rules
     * @throws ValidationException
     */
    public static function validate(array $params, array $rules) {

        foreach ($rules as $key => $rule) {
            foreach ($rule as $validator) {
                self::{$validator}($params[$key] ?? null);
            }
        }
        if (isset($_SESSION['validation'])) {
            throw new ValidationException();
        }
    }



    protected static function email(?string $email):void {
        if (is_null($email)) {
            $_SESSION['validation']['email'] = 'Niewłaściwy email';

        }

        $email = filter_var(filter_var($email, FILTER_SANITIZE_EMAIL), FILTER_VALIDATE_EMAIL);

        if (!$email) {
            $_SESSION['validation']['email'] = 'Niewłaściwy email';
        }
    }


    protected static function required(?string $param):void {
        if (is_null($param) || empty($param)) {
            $_SESSION['validation']['required'] = 'Pole '.$param.' jest wymagane!';
        }
    }

    protected static function password(?string $password):void  {
        if (is_null($password) || strlen($password) < 8) {
            $_SESSION['validation']['password'] = 'Hasło ma mniej niż 8 znaków!';
        }

    }

    /*protected static function image(?string $file) {
        if (!is_null($file)) {
            $image = $_FILES[$file] ?? null;

            if (!is_null($image) && strpos($image['type'], 'image') === false) {
                $_SESSION['validation']['image'] = 'Nieprawidłowe zdjęcie';
            }
        }
    }*/
}