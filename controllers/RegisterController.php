<?php

namespace Controllers;

use Bootstrap\Auth;
use Bootstrap\Validator;
use Models\User;

class RegisterController extends Controller
{
    protected $user;

    public function __construct()
    {
        parent::__construct();
        $this->user = new User();
    }

    public function index() {
        if (Auth::check()) {
            redirect();
        }
        return $this->view('register.register');
    }

    public function register() {
        if (Auth::check()) {
            redirect();
        }

        try {
            Validator::validate($_POST, [
                'email' => ['required', 'email'],
                'password' => ['required', 'password'],
            ]);

            $email = $_POST['email'];
            $password = $_POST['password'];

            $user = $this->user->createNewUser($email, $password);
            if (is_null($user)) {
                return $this->view('register.register', [
                    'errors' => ['email_exist' => 'email już istnieje'],
                    'form_data' => $_POST,
                ]);
            }

            Auth::attempt([
                'email' => $email,
                'password' => $password,
            ]);
            redirect();
        } catch (\Exception $exception) {
            $errors = $_SESSION['validation'];
            unset($_SESSION['validation']);
            return $this->view('register.register', [
                'errors' => $errors,
                'form_data' => $_POST,
            ]);
        }
    }
}

