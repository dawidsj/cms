<?php

namespace Bootstrap;

class Model
{
    protected $connection;

    public function __construct() {
        try {

            $this->connection = new \PDO("mysql:host=".env('DB_HOST').";dbname=".env('DB_DATABASE').";charset=utf8", env('DB_USERNAME'), env('DB_PASSWORD'));

        } catch (\PDOException $error) {
            echo $error;
            exit('Model error');
        }
    }
}