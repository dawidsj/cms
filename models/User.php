<?php

namespace Models;

use Bootstrap\Model;

class User extends Model
{
    public function getUser(string $email):?array {
        $query = $this->connection->prepare('SELECT * FROM users WHERE email= :email');
        $query->bindValue(':email', $email, \PDO::PARAM_STR);
        $query->execute();

        $user = $query->fetch(\PDO::FETCH_ASSOC);

        if ($user) {
            return $user;
        }
        return null;
    }
    public function createNewUser(string $email, string $password):?array {
        if (is_null($this->getUser($email))) {
            $hash = password_hash($password, PASSWORD_DEFAULT);

            $query = $this->connection->prepare('INSERT INTO users VALUES (NULL, :email, :hashedPassword, 0)');
            $query->bindValue(':email', $email, \PDO::PARAM_STR);
            $query->bindValue(':hashedPassword', $hash, \PDO::PARAM_STR);
            $query->execute();

            return $this->getUser($email);
        }
        return null;
    }
}