<?php

namespace Controllers\Admin;

use Bootstrap\Auth;
use Controllers\Controller;
use Models\Article;

class AdminController extends Controller
{
    protected $articles;

    public function __construct()
    {
        if (!Auth::isAdmin()) {
            redirect();
        }
        parent::__construct();
        $this->articles = new Article();
    }

    public function index() {
        $articles = $this->articles->getArticles();

        return $this->view('admin.dashboard', [
            'articles' => $articles,
        ]);
    }

}