@extends('layouts.layout')
@section('css')
<link rel="stylesheet" href="<?php echo route('resources/assets/css/article.css'); ?>">
@endSection('css')
@section('title')

<?php
echo $this->articles['article_title'];
?>
@endSection('title')


@section('content')
<main>
    <section class="mainHeader">
        <div class="container">
            <h1 class="mx-0"><?= $this->articles['article_title'] ?></h1>
        </div>
    </section>
    <section class="mainContent">
        <div class="container">
            <p>
                <?= $this->articles['article_content'] ?>
            </p>
            <div class="col-10 mx-auto">
                <div class="imageInArticle">
                    <img src="<?= route('storage/images/').$this->articles['image'] ?>" class="rounded mx-auto d-block" style="" alt="">
                </div>
            </div>
            <br/>
            <p style="font-size: smaller; text-align: right">
                <i>
                    Utworzono: <?= $this->articles['created_at'] ?><br/>
                    Autor: <?= $this->articles['email'] ?>
                </i>
            </p>

        </div>
    </section> <br/><br/>
    <section class="comments">
        <div class="container">

            <p style="font-size: larger; text-align: center "><b>Komentarze</b></p>

            <?php
                foreach ($this->comments as $comment) {
                    echo '<p><i><b>'.$comment['email'].'</b></i></p>';
                    echo '<p>'.$comment['comment'].'</p>';
                    echo '<p style="font-size: x-small"><i>'.$comment['created_at'].'</i></p>';
                    echo '<hr class="col-3 ml-0" style="border-color: #C3073F">';
                }
                if (empty($comment)) {
                    echo '<hr class="col-10" style="border-color: #C3073F">';
                    echo '<p style="text-align: center">Pusto tutaj :o</p>';
                    echo '<hr class="col-10" style="border-color: #C3073F">';

                }
            ?>

        </div>
        <div class="container">
            <div class="col-6 mx-auto">
            <p style="text-align: center; font-size: larger "><b>Dodaj swój komentarz!</b></p>
            <form method="post" action="<?= route('articles/store/comment') ?>">
                <textarea class="form-control" id="comment" name="comment" rows="3"></textarea><br/>
                <input type="submit" class="btn col-4 mx-auto" value="Prześlij!">
            </form>
            </div>
        </div>

    </section>
</main>



<img src="<?php echo \Bootstrap\Storage::getFilePath($this->articles['image']); ?>" alt="">
@endSection('content')
