<?php

namespace Bootstrap;

use Models\User;

final class Auth
{
    public static function check():bool {
        return isset($_SESSION['user']);
    }

    public static function user():?array {
        if (self::check()) {
            return $_SESSION['user'];
        }
        return null;
    }

    public static function attempt(array $params):?array {
        if (self::check()) {
            return self::user();
        }

        $user = (new User())->getUser($params['email']);
        if (!is_null($user) && password_verify($params['password'], $user['password'])) {
            $_SESSION['user'] = $user;
            return $user;
        }
        return null;
    }

    public static function logout() {
        session_destroy();
    }

    public static function isAdmin():bool {
        $user = self::user();
        if (is_null($user)) {
            return false;
        }
        return (bool) $user['is_admin'];
    }

}