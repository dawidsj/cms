<?php

namespace Models;

use Bootstrap\Auth;
use Bootstrap\Model;

class Article extends Model
{
    const STATUS_WAITING = 0;
    const STATUS_ACCEPTED = 1;
    const STATUS_REJECTED = 2;

    public function getArticles():array {
        $query = $this->connection->prepare('SELECT articles.*, users.email FROM articles INNER JOIN users ON articles.author = users.id ORDER BY created_at DESC');
        $query->execute();

        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getArticlesByStatus(int $status):array {
        $query = $this->connection->prepare('SELECT * FROM articles WHERE status = :status ORDER BY created_at DESC');
        $query->bindValue(':status', $status);
        $query->execute();

        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function findArticle(int $id):array {
        $query = $this->connection->prepare('SELECT articles.*, users.email FROM articles INNER JOIN users WHERE articles.article_id = :article_id AND articles.author = users.id');
        $query->bindValue(':article_id', $id);
        $query->execute();

        $result = $query->fetchAll(\PDO::FETCH_ASSOC);
        if (!empty($result)) {
            return $result[0];
        }
        return [];
    }

    public function createArticle(array $article):void {
        $query = $this->connection->prepare('INSERT INTO articles VALUES (null, :author, :article_title, :article_content, :article_image, CURRENT_TIME, :article_status)');
        $query->bindValue(':author', Auth::user()['id']);
        $query->bindValue(':article_title', $article['title']);
        $query->bindValue(':article_content', $article['content']);
        $query->bindValue(':article_image', $article['image']);
        $query->bindValue(':article_status', $article['status']);
        $query->execute();
    }

    public function updateArticle(int $id, array $article):void {
        $query = $this->connection->prepare('UPDATE articles SET article_title = :title, article_content = :content, article_image = :article_image WHERE article_id = :id');
        $query->bindValue(':title', $article['title']);
        $query->bindValue(':content', $article['content']);
        $query->bindValue(':article_image', $article['image']);
        $query->bindValue(':id', $id);
        $query->execute();
    }

    public function changeStatus(int $id, int $status):void {
        $query = $this->connection->prepare('UPDATE articles SET status = :status WHERE article_id = :id');
        $query->bindValue(':status', $status);
        $query->bindValue(':id', $id);
        $query->execute();
    }

    public function getUserArticles(int $id):array {
        $query = $this->connection->prepare('SELECT * FROM articles WHERE author = :author');
        $query->bindValue(':author', $id);
        $query->execute();

        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getArticleComments(int $id):array {
        $query = $this->connection->prepare('SELECT comments.*, articles.article_id, articles.author, articles.article_title, users.email FROM 
                                                    (comments INNER JOIN articles ON comments.article_id = articles.article_id) 
                                                    INNER JOIN users ON users.id = comments.comment_author WHERE articles.article_id = :articleId');
        $query->bindValue(':articleId', $id);
        $query->execute();

        $result = $query->fetchAll(\PDO::FETCH_ASSOC);
        if (!empty($result)) {
            return $result;
        }
        return [];
    }

    public function createNewComment (string $comment) {
        $query = $this->connection->prepare('INSERT INTO comments VALUES (NULL, article_id, comment_author, comment, CURRENT_TIME)');

    }


}