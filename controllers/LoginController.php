<?php

namespace Controllers;

use Bootstrap\Auth;
use Bootstrap\Validator;

class LoginController extends Controller
{
    public function index() {
        if (Auth::check()) {
            redirect();
        }
        return $this->view('login.login', [
            'xd' => 'Welcome here'
        ]);
    }

    public function login() {
        if (Auth::check()) {
            redirect();
        }

        try {
            Validator::validate($_POST, [
                'email' => ['required', 'email'],
                'password' => ['required'],
            ]);

            $email = $_POST['email'];
            $password = $_POST['password'];

            $user =  Auth::attempt([
                'email' => $email,
                'password' => $password,
            ]);

            if (is_null($user)) {
              return $this->view('login.login', [
                  'errors' => ['bad_credentials' => 'Login i hasło nie pasują do siebie!'],
                  'form_data' => $_POST,
              ]);
            }

            redirect();

        } catch (\Exception $exception) {
            $errors = $_SESSION['validation'];
            unset($_SESSION['validation']);
            return $this->view('login.login', [
                'errors' => $errors,
                'form_data' => $_POST,
            ]);
        }
    }

    public function logout() {
        Auth::logout();
        redirect($_SESSION['previous_route'] ?? '');
    }
}






