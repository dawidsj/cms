@extends('layouts.layout')

@section('title')
Panel zarządzania
@endSection('title')


@section('content')

<main>
    <section class="mainHeader">
        <div class="container">
            <h1 class="mx-0"><i class="fab fa-accessible-icon"></i> Panel zarządzania</h1><br/>
            <h2>Lista artykułów</h2>
        </div>
    </section>
    <section class="mainContent">
        <div class="container">
            <table class="table table-striped table-dark" >
                <thead class="thead-dark">
                <tr>
                    <th>#</th>
                    <th>Id artykułu</th>
                    <th>Id autora</th>
                    <th>Email autora</th>
                    <th>Tytuł</th>
                    <th>Data utworzenia</th>
                    <th>Status</th>
                    <th>Akcje</th>
                </tr>
                </thead>
                <tbody>
                    <?php

                    foreach ($this->articles as $index => $article) {
                        $index+=1;
                        echo '<tr>';
                            echo '<td>'.$index.'</td>';
                            echo '<td>'.$article['article_id'].'</td>';
                            echo '<td>'.$article['author'].'</td>';
                            echo '<td>'.$article['email'].'</td>';
                            echo '<td>'."<a href=".route("articles/".$article['article_id']).">".$article['article_title']."</a>".'</td>';
                            echo '<td>'.$article['created_at'].'</td>';

                            if($article['status'] == \Models\Article::STATUS_WAITING) {
                                echo '<td>'.'<span style="color: yellow"> Oczekujący </span>'.'</td>';
                            } else if ($article['status'] == \Models\Article::STATUS_ACCEPTED) {
                                echo '<td>'.'<span style="color: green"> Zaakceptowany </span>'.'</td>';
                            } else if ($article['status'] == \Models\Article::STATUS_REJECTED) {
                                echo '<td>'.'<span style="color: red"> Odrzucony </span>'.'</td>';
                            }
                        echo '<td>'."<a href=".route("articles/edit/".$article['article_id'])."><i class=\"fas fa-pencil-alt\"></i> </a>".'</td>';
                    }

                    ?>
                </tbody>
            </table>
        </div>
    </section>
</main>



@endSection('content')
