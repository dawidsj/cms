@extends('layouts.layout')

@section('title')

Artykuły
@endSection('title')


@section('content')
<main>
    <section class="mainHeader">
        <div class="container">
            <?php
            if ($_SERVER['REQUEST_URI'] == '/cms_obiektowy/articles/create' || $_SERVER['REQUEST_URI'] == '/cms_obiektowy/articles/store') {
                echo '<h1 class="mx-0"><i class="fas fa-newspaper"></i> Dodaj nowy artykuł</h1>';
            } else {
                echo '<h1 class="mx-0"><i class="fas fa-pencil-alt"></i> Edytuj artykuł</h1>';
            }
            ?><br/>
        </div>
    </section>
    <section class="mainContent">
        <div class="container">
            <form method="post" class="col-10 mx-auto" action="<?= isset($this->article) ? route('articles/update').'/'.$this->article['article_id'] : route('articles/store')?>" enctype="multipart/form-data">
                <label>Tytuł</label>
                <input type="text" name="title" class="form-control" value="<?php
                if (isset($this->form_data['title'])) {
                    echo $this->form_data['title'];
                }
                else if (isset($this->article['article_title'])) {
                    echo $this->article['article_title'];
                } else {
                    echo '';
                }
                ?>">
                <div class="<?php if (strlen($this->errors['required']) > 0) echo 'alert alert-danger col-12'; else echo ' ';?>">
                    <?php echo $this->errors['required'] ?? ''; ?>
                </div>
                <br/>
                <label>Treść</label>
                <textarea id="editor" name="content" rows="4" cols="50"><?= isset($this->article) ? $this->article['article_content'] : ''  ?></textarea><br/>

                <?php if (\Bootstrap\Auth::isAdmin()) { ?>
                    <label>Status:</label><br/>
                        <select name="status" class="custom-select">
                            <option value="<?= \Models\Article::STATUS_WAITING ?>"<?= isset($this->article['status']) && $this->article['status'] == \Models\Article::STATUS_WAITING ? 'selected'  : '' ?>>Oczekujący</option>
                            <option value="<?= \Models\Article::STATUS_ACCEPTED ?>"<?= isset($this->article['status']) && $this->article['status'] == \Models\Article::STATUS_ACCEPTED ? 'selected'  : '' ?>>Zaakceptowany</option>
                            <option value="<?= \Models\Article::STATUS_REJECTED ?>"<?= isset($this->article['status']) && $this->article['status'] == \Models\Article::STATUS_REJECTED ? 'selected'  : '' ?>>Odrzucony</option>
                        </select>

                <?php } ?>
                <br/><br/>
                <label>Prześlij zdjęcie</label>
                <input type="file" class="form-control-file" name="image" <?= isset($this->article) ?  '' : 'required' ?>> <br/><br/>
                <div class="row">
                <input type="submit" class="btn col-4 mx-auto" value="Prześlij!">
                </div>
            </form>
        </div>
    </section>

@endSection('content')
@section('js')
<script>
    ClassicEditor
        .create( document.querySelector( '#editor' ) )
        .then( editor => {
            console.log( editor );
        } )
        .catch( error => {
            console.error( error );
        } );
</script>
@endSection('js')