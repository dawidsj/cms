@extends('layouts.layout')


@section('content')

<main>
    <section class="mainHeader">
        <div class="container">
            <h1 class="mx-0">Logowanie</h1>
        </div>
    </section>
    <section class="loginPanel">
        <div class="container">
            <form method="post">
                Email: <input type="text" name="email" class="form-control col-4 mx-auto my-1" value="<?php echo $this->form_data['email'] ?? ''; ?>">
                <div class="<?php if (strlen($this->errors['bad_credentials']) > 0) echo 'alert alert-danger col-4 mx-auto'; else echo ' ';?>">
                <?php echo $this->errors['bad_credentials'] ?? ''; ?>
                </div>
                <br/>
                Hasło: <input type="password" name="password" class="form-control col-4 mx-auto my-1"> <br>
                <input type="submit" value="Zaloguj!" class="btn col-2">
            </form>
        </div>
    </section>
</main>


@endSection('content')