<?php

namespace Controllers;

class HomeController extends Controller
{
    function index() {
        return $this->view('home.index');
    }
}