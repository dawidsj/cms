@extends('layouts.layout')

formularz rejestracji

@section('content')
<main>
    <section class="mainHeader">
        <div class="container">
            <h1 class="mx-0">Rejestracja</h1>
        </div>
    </section>
    <section class="loginPanel">
        <div class="container">
            <form method="post">
                Email: <br> <input type="text" name="email" class="form-control col-4 mx-auto my-1" value="<?php echo $this->form_data['email'] ?? ''; ?>">
                <div class="<?php if (strlen($this->errors['email']) > 0 || strlen($this->errors['email_exist']) > 0 ) echo 'alert alert-danger col-4 mx-auto'; else echo ' ';?>">
                    <?php echo $this->errors['email'] ?? ''; ?>
                    <?php echo $this->errors['email_exist'] ?? ''; ?>
                </div>
                <br/>
                Hasło: <br> <input type="password" name="password" class="form-control col-4 mx-auto my-1">
                <div class="<?php if (strlen($this->errors['password']) > 0) echo 'alert alert-danger col-4 mx-auto'; else echo ' ';?>">
                    <?php echo $this->errors['password'] ?? ''; ?>
                </div>
                <br/>
                <input type="submit" value="Zarejestruj!" class="btn col-2" style="background-color: #C3073F; color: white;">

            </form>
        </div>
    </section>
</main>


@endSection('content')



