<?php

if (!function_exists('dd')) {
    function dd(...$args) {
        foreach ($args as $arg) {
            echo '<div style="background-color:black; color:orange;">';
            var_dump($arg);
            echo '</div>';
            echo '<br/>';
        }
        die();
    }
}
if (!function_exists('dump')) {
    function dump(...$args) {
        foreach ($args as $arg) {
            echo '<div style="background-color:black; color:lightseagreen;">';
            var_dump($arg);
            echo '</div>';
            echo '<br/>';
        }
    }
}

if (!function_exists('string_between_two_string')) {
    function string_between_two_string($str, $starting_word, $ending_word)
    {
        if (strpos($str, $starting_word) === false || strpos($str, $ending_word) === false) {
            return '';
        }
        $substring_start = strpos($str, $starting_word);
        $substring_start += strlen($starting_word);
        $size = strpos($str, $ending_word, $substring_start) - $substring_start;

        return substr($str, $substring_start, $size);
    }
}

if (!function_exists('redirect')) {
    function redirect(string $location = null)
    {
        if (is_null($location)) {
            header( "Location: ".env('APP', 'http://localhost'));
        }
        header( "Location: ".env('APP', 'http://localhost').'/'.$location);
    }
}

if(!function_exists('env')) {
    function env($key, $default = null)
    {
        $value = getenv($key);
        if (!$value) {
            return $default;
        }
        return $value;
    }
}

if(!function_exists('route')) {
    function route($route)
    {
        $app = env('APP');

        return $app.'/'.$route;
    }
}

if(!function_exists('generateRandomString')) {
    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
