<?php

namespace Bootstrap;

class View
{
    private $layout;
    private $view;
    private $fields;
    private $sections;

    public function render(string $name, ?array $params) {
        if (!is_null($params)) {
            $this->setParams($params);
        }
        $this->view = $this->getContent($this->getFullName($name));
        $view = $this->prepareView();

        echo $view;
    }

    private function setParams(array $params): void {
        foreach ($params as $key => $value) {
            $this->{$key} = $value;
        }

    }

    private function getFullName(string $name): string {
        $name = str_replace('.', '/', $name);
        return 'resources/views/'.$name.'.php';
    }

    private function getContent(string $path): string {
        ob_start();
        require_once($path);
        $content = ob_get_contents();
        ob_end_clean();

        return $content;
    }

    private function prepareView() {
        if (strpos($this->view, '@extends') !== false) {
            $layoutName = substr($this->view, strpos($this->view, "@extends")+10);
            $layoutName = substr($layoutName, 0, strpos($layoutName, "')"));
            $this->layout = $this->getContent($this->getFullName($layoutName));
            $this->findFields();
            $this->findSections();

            return $this->replaceSections();
        }
        return $this->view;
    }

    private function findFields(): array {
        $haystack = $this->layout;
        $lastPos = 0;
        $needle = '@field';
        $fields = [];

        while (($lastPos = strpos($haystack, $needle, $lastPos))!== false) {
            $lastPos = $lastPos + strlen($needle);
            $fieldName = substr($haystack, strpos($haystack, $needle)+8);
            $fieldName = substr($fieldName, 0, strpos($fieldName, "')"));
            $fields[] = $fieldName;
            $haystack = substr($haystack, $lastPos);
            $lastPos = 0;
        }

        $this->fields = $fields;
        return $fields;
    }

    private function findSections(): array {
        if (is_null($this->fields)) {
            return [];
        }

        foreach ($this->fields as $field) {
            $start = "@section('{$field}')";
            $stop = "@endSection('{$field}')";

            $content = string_between_two_string($this->view, $start, $stop);
            $this->sections[$field] = $content;
        }
        return $this->sections;
    }

    private function replaceSections(): string {
        $layout = $this->layout;
        foreach ($this->fields as $field) {
            $toReplace = "@field('{$field}')";

            $layout = str_replace($toReplace, $this->sections[$field], $layout);
        }
        return $layout;
    }
}