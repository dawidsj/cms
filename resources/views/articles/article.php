@extends('layouts.layout')
@section('css')
<link rel="stylesheet" href="<?php echo route('resources/assets/css/article.css'); ?>">
@endSection('css')
@section('title')

Artykuły
@endSection('title')

@section('content')


<main>
    <section class="mainHeader">
        <div class="container">
            <h1 class="mx-0"><i class="fas fa-newspaper"></i> Lista artykułów</h1>
        </div>
    </section>
    <section class="mainContent">
        <div class="container">
            <div class="row justify-content-end col-11 px-0 mx-0">
                <?= (\Bootstrap\Auth::check()) ? '<a href="'.route('articles/my-articles').'" class="btn col-2 mr-3"><i class="fas fa-list"></i>  Moje artykuły</a>' : ''?>
                <?= (\Bootstrap\Auth::check()) ? '<a href="'.route('articles/create').'" class="btn col-2"><i class="fas fa-plus"></i> Nowy artykuł!</a>' : ''?>
            </div><br/>

            <?php

            foreach ($this->articles as $index => $article) {
                echo '<article class="article col-10 mx-auto">';
                    echo '<div class="row mx-0">';
                        if ($index % 2 == 0) {

                            echo '<div class="image">';
                            echo '<a href="'.route("articles/".$article['article_id']).'">'.'<img src="'.route('storage/images/').$article['image'].'" alt="" >'.'</a>';
                            echo '</div>';
                            echo '<div class="post">';
                            echo '<h3>'."<a href=".route("articles/".$article['article_id']).">".$article['article_title']."</a>".'</h3>';
                            echo '<p>'.implode(' ', array_slice(explode(' ', $article['article_content']), 0, rand(15,25))).'<a href="'.route("articles/".$article['article_id']).'">'.'<i>... Czytaj dalej</i>'.'</a>'.'</p>';
                            echo '</div>';
                        }
                        else {
                            echo '<div class="post">';
                            echo '<h3>'."<a href=".route("articles/".$article['article_id']).">".$article['article_title']."</a>".'</h3>';
                            echo '<p>'.implode(' ', array_slice(explode(' ', $article['article_content']), 0, rand(15,25))).'<a href="'.route("articles/".$article['article_id']).'">'.'<i> ...Czytaj dalej</i>'.'</a>'.'</p>';
                            echo '</div>';
                            echo '<div class="image">';
                            echo '<a href="'.route("articles/".$article['article_id']).'">'.'<img src="'.route('storage/images/').$article['image'].'" alt="" >'.'</a>';

                            echo '</div>';
                        }
                    echo '</div>';
                echo '</article>';

                echo '<br/>';
            }
            ?>
        </div>
    </section>
</main>



@endSection('content')
