<?php


namespace Bootstrap;


class Storage
{
    const STORAGE_LOCATION = 'storage/images';

    public static function save(array $file):string {
        $imageFileType = strtolower(pathinfo($file['name'],PATHINFO_EXTENSION));
        $name = generateRandomString(32).'.'.$imageFileType;

        $tmpFile = $file['tmp_name'];

        move_uploaded_file($tmpFile, self::getFilePath($name));
        return $name;
    }

    public static function getFilePath(string $fileName):string {
        return self::STORAGE_LOCATION.'/'.$fileName;
    }

}