<?php

namespace Bootstrap;

use Routes\Router;

final class App
{
    protected $class;
    protected $method;
    protected $routes;
    protected $attributes;

    public function __construct()
    {
        $path = $_GET['path'] ?? '';
        $this->recognizeRequest();

        $route = $this->recognizeRoute($path);
        if (!is_null($route)) {
            $this->recognizeClassAndMethod($route);
            $this->load();
            return;
        }

        echo 'Route not provided';
    }


    private function recognizeClassAndMethod(string $path) {
        $classAndMethod = explode("@", $path);

        $this->class = $classAndMethod[0];
        $this->method = $classAndMethod[1];
    }

    private function load() {
        $className = 'Controllers\\'.$this->class;
        $class = new $className;

        if (!is_null($this->attributes)) {
            $class->{$this->method}(...$this->attributes);
        } else {
            $class->{$this->method}();
        }
    }

    private function recognizeRequest() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->routes = Router::post();
        }
        else {
            $this->routes = Router::get();
        }

    }

    private function recognizeRoute(string $path):?string {
        if (array_key_exists($path, $this->routes)) {
            return $this->routes[$path];
        }
        foreach ($this->routes as $route => $class) {
            if (strpos($route, '{') !== false) {
                $explodedPath = explode("/", $path);
                $explodedRoute = explode("/", $route);

                if (count($explodedPath) === count($explodedRoute)) {
                    foreach ($explodedRoute as $index => $partOfRoute) {
                        if (strpos($partOfRoute,"{") !== false) {
                            $this->attributes[] = $explodedPath[$index];
                        } elseif($explodedPath[$index] !== $explodedRoute[$index]) {
                            $this->attributes = null;
                            break;
                        }
                    }
                }
            }
            if (!is_null($this->attributes)) {
                return $class;
            }
        }

        return null;
    }


}

