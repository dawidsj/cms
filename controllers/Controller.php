<?php

namespace Controllers;

use Bootstrap\View;

abstract class Controller
{
    private $view;

    public function __construct() {
        $this->view = new View();
    }

    /**
     * @param string $name
     * @param array|null $params
     * @return true
     */
    public function view(string $name, array $params = null): bool {
        $this->view->render($name, $params);

        return true;
    }
}

