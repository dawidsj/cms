<html>
<head>
<title>@field('title')</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo route('resources/assets/css/layout.css'); ?>">

    @field('css')

    <script src="https://kit.fontawesome.com/9ac5b7cbb5.js" crossorigin="anonymous"></script>
    <script src="https://cdn.ckeditor.com/ckeditor5/16.0.0/classic/ckeditor.js"></script>
</head>
<body>
<header>
    <nav class="navbar">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="<?= route('') ?>">
                    <i class="fas fa-home"></i> Strona główna
                </a>

            </div>
            <div class="mx-auto">
                <a href="<?php echo route('articles') ?>" class=""><i class="fas fa-newspaper"></i> Artykuły</a>
            </div>
            <div class="mr-sm-2">
            <?php
            if (!\Bootstrap\Auth::check()) {
                echo '<a href="'.route('login').'" class="rightBtn"><i class="fas fa-sign-in-alt"></i> Logowanie   </a>';
                echo '<a href="'.route('register').'" class="rightBtn"><i class="fas fa-user"></i> Rejestracja</a>';
            }

            if (\Bootstrap\Auth::isAdmin()) {
                echo '<a href="'.route('admin/dashboard').'" class="rightBtn"><i class="fab fa-accessible-icon"></i> Panel zarządzania </a>';
            }

            if (\Bootstrap\Auth::check()) {
                echo '<a href="'.route('logout').'" class="rightBtn"><i class="fas fa-sign-out-alt"></i> Wyloguj </a>';
            }

            ?>
            </div>
        </div>
    </nav>
</header>


@field('content')

@field('js')

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>