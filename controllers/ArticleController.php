<?php

namespace Controllers;


use Bootstrap\Auth;
use Bootstrap\Storage;
use Bootstrap\Validator;
use Models\Article;

class ArticleController extends Controller
{
    protected $articles;

    public function __construct()
    {
        parent::__construct();
        $this->articles = new Article();
    }

    public function index() {
        $articles = $this->articles->getArticlesByStatus(Article::STATUS_ACCEPTED);
        return $this->view('articles.article', [
            'articles' => $articles,
        ]);
    }

    public function article($id) {

        $article = $this->articles->findArticle($id);
        if (empty($article)) {
            return redirect('articles');
        }

        $comment = $this->articles->getArticleComments($id);
        $getback = $_GET['path'];

        return $this->view('articles.read', [
            'articles' => $article,
            'comments' => $comment,
        ]);
    }


    public function create() {
        if (!Auth::check()) {
            return redirect();
        }

        return $this->view('articles.form');
    }

    public function edit (int $id) {
        $article = $this->articles->findArticle($id);
        if (empty($article) || !$this->canEdit($article)) {
            return redirect();
        }

        return $this->view('articles.form', [
            'article' => $article,
        ]);
    }

    public function update(int $id) {
        $article = $this->articles->findArticle($id);
        if (empty($article) || !$this->canEdit($article)) {
            return redirect();
        }

        try {
            Validator::validate($_POST, [
                'title' => ['required'],
                'content' => ['required'],
            ]);
            if (isset($_POST['status']) && Auth::isAdmin()) {
                $this->articles->changeStatus($id, $_POST['status']);
            }
            $this->articles->updateArticle($id, $_POST);

            return redirect('articles');

        } catch (\Exception $exception) {
            $errors = $_SESSION['validation'];
            unset($_SESSION['validation']);
            return $this->view('articles.form', [
                'errors' => $errors,
                'form_data' => $_POST,
            ]);
        }

    }

    protected function getNewArticleStatus() {
        if (!Auth::isAdmin()) {
            $status = Article::STATUS_WAITING;
        } else {
            $status = $_POST['status'];
        }
        return $status;
    }

    public function store() {
        if (!Auth::check()) {
            return redirect();
        }
        try {
            Validator::validate($_POST, [
                'title' => ['required'],
                'content' => ['required'],
            ]);
            $imageName = Storage::save($_FILES['image']);
            $this->articles->createArticle(array_merge($_POST, ['image' => $imageName], ['status' => $this->getNewArticleStatus()]));
            return redirect('articles');

        } catch (\Exception $exception) {
            $errors = $_SESSION['validation'];
            unset($_SESSION['validation']);
            return $this->view('articles.form', [
                'errors' => $errors,
                'form_data' => $_POST,
            ]);
        }
    }

    public function myArticles() {
        $user = Auth::user();
        if (is_null($user)) {
            return redirect();
        }
        $articles = $this->articles->getUserArticles($user['id']);

        return $this->view('articles.my-articles', [
            'articles' => $articles,
        ]);

    }


    protected function canEdit(array $article):bool {
        if (!Auth::check()) {
            return false;
        }
        if (Auth::isAdmin()) {
            return true;
        }

        return $article['author'] == Auth::user()['id'] && $article['status'] != Article::STATUS_ACCEPTED;
    }

    public function storeComment() {
        if (!Auth::check()) {
            return redirect();
        }
        try {
            Validator::validate($_POST, [
                'comment' => ['required'],
            ]);

            $this->articles->createNewComment($_POST);
        } catch (\Exception $exception) {

        }
        return redirect('articles');
    }
}