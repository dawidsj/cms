@extends('layouts.layout')

@section('title')
Artykuły
@endSection('title')


@section('content')

<main>
    <section class="mainHeader">
        <div class="container">
            <h1 class="mx-0"><i class="fas fa-list"></i> Moje artykuły</h1>
        </div>
    </section>
    <section class="mainContent">
        <div class="container">
            <div class="row justify-content-end col-12 px-0 mx-0">
                <?= (\Bootstrap\Auth::check()) ? '<a href="'.route('articles/create').'" class="btn col-2"><i class="fas fa-plus"></i> Nowy artykuł!</a>' : ''?>
            </div><br/>
            <table class="table table-striped table-dark" >
                <thead class="thead-dark">
                <tr>
                    <th>#</th>
                    <th>Id</th>
                    <th>Tytuł</th>
                    <th>Data utworzenia</th>
                    <th>Status</th>
                    <th>Akcje</th>
                </tr>
                </thead>
                <tbody>
                    <?php
                    $i=1;
                        foreach ($this->articles as $article) {
                            echo '<tr>';
                                echo '<th>'.$i.'</th>'; $i+=1;
                                echo '<td>'.$article['article_id'].'</td>';
                                echo '<td>'."<a href=".route("articles/".$article['article_id']).">".$article['article_title']."</a>".'</td>';
                                echo '<td>'.$article['created_at'].'</td>';

                                if($article['status'] == \Models\Article::STATUS_WAITING) {
                                    echo '<td>'.'<span style="color: yellow"> Oczekujący </span>'.'</td>';
                                } else if ($article['status'] == \Models\Article::STATUS_ACCEPTED) {
                                    echo '<td>'.'<span style="color: green"> Zaakceptowany </span>'.'</td>';
                                } else if ($article['status'] == \Models\Article::STATUS_REJECTED) {
                                    echo '<td>'.'<span style="color: red"> Odrzucony </span>'.'</td>';
                                }
                                if ($article['status'] != \Models\Article::STATUS_ACCEPTED || \Bootstrap\Auth::isAdmin()) {
                                    echo '<td>'."<a href=".route("articles/edit/".$article['article_id'])."><i class=\"fas fa-pencil-alt\"></i> </a>".'</td>';
                                }
                                else
                                    echo '<td>'.'</td>';

                            echo '</tr>';
                        }
                    ?>

                </tbody>
            </table>
        </div>
    </section>
</main>



@endSection('content')
