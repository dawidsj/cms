@extends('layouts.layout')

@section('css')

@endSection('css')

@section('title')
Strona główna
@endSection('title')


@section('content')

<main>
    <section class="mainHeader">
        <div class="container">
                <h1 class="mx-0">Witaj na stronie głównej!</h1>
        </div>
    </section>
    <section class="mainContent">
        <div class="container">
            <h3>Pare słów o...</h3>
            <span>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec luctus, orci vel auctor ultrices, quam dui congue lorem, in vestibulum nunc risus a augue. Suspendisse cursus nisl in pulvinar maximus. Quisque porttitor eros elit, at convallis tortor scelerisque in. Quisque et nibh eget elit posuere commodo. Vestibulum vel ipsum felis. Duis sapien nibh, consequat eu fermentum at, auctor id nibh. Pellentesque posuere scelerisque dolor eget consectetur. Vivamus luctus, ex ac aliquet mollis, enim eros tempus lacus, et vehicula augue metus ac elit. Donec ornare leo in augue rhoncus, sit amet tincidunt diam eleifend. Suspendisse elementum eu neque ac euismod. Integer feugiat vestibulum turpis ac aliquam. Mauris ut ipsum aliquet, tincidunt purus sit amet, auctor libero.
                Pellentesque vehicula aliquet suscipit. Nam dictum magna ut rutrum fringilla. Proin eleifend nisi at orci tincidunt pulvinar. Nunc commodo, felis accumsan consequat pulvinar, ligula urna bibendum enim, sed scelerisque sem risus at felis. In lacinia, quam non sagittis venenatis, tellus nunc viverra nibh, pharetra molestie purus augue vel lorem. Phasellus sed libero vestibulum, egestas est id, mattis enim. Ut commodo enim finibus orci pellentesque, ac ultricies nunc vulputate. Sed faucibus lacus ut dolor mattis efficitur. Etiam ac scelerisque tortor. Curabitur condimentum sagittis leo, vel scelerisque lectus laoreet a. Nulla mattis lacus nulla, sit amet tristique mi scelerisque ac. Morbi velit urna, pretium nec tincidunt at, ullamcorper at mauris. Nam pellentesque, est ut imperdiet auctor, ante felis convallis lorem, a egestas nibh augue ut diam.
                Sed dignissim felis vel turpis feugiat fringilla. Ut quis ipsum non lacus dignissim dignissim vitae eu dolor. Praesent euismod odio nec risus mattis dignissim. Sed tellus augue, rhoncus quis erat non, eleifend sollicitudin lorem. Ut neque purus, posuere id nulla eu, bibendum gravida libero. Ut volutpat sapien ultrices ultrices aliquet. Sed maximus, enim sed dictum dapibus, eros lectus ultricies mauris, sed fringilla dolor magna eget quam. Vivamus mattis dui ac ex ultricies dictum.
                Phasellus posuere libero a nunc pellentesque, vel rutrum ante aliquet. Aliquam erat volutpat. Proin ornare massa quis ante tincidunt, vel aliquam enim ullamcorper. Aliquam tortor mi, eleifend in purus id, gravida egestas sapien. Suspendisse posuere malesuada lectus, et hendrerit lectus porttitor sit amet. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nulla libero massa, finibus quis condimentum a, dapibus a magna. Praesent commodo nibh id justo lacinia, quis egestas urna interdum. Vivamus et nunc mi. Nam et porttitor purus. Mauris sed mi volutpat, blandit turpis eu, imperdiet est. Nunc ex neque, efficitur nec hendrerit nec, molestie euismod quam. Suspendisse potenti.
                In sit amet gravida risus. Donec maximus, mi eget elementum malesuada, neque dui auctor tortor, a euismod massa nisi ut dui. Duis at urna at risus interdum luctus. Ut ante lorem, egestas at mauris eu, eleifend volutpat lorem. Interdum et malesuada fames ac ante ipsum primis in faucibus. Mauris nisi nunc, lacinia ut elit vitae, semper tincidunt felis. Fusce semper dictum congue. Vivamus tincidunt, neque in tempus elementum, nisl felis tincidunt arcu, at rutrum eros lacus non quam. In feugiat augue vel nisl pulvinar, et ullamcorper diam molestie. Cras lacinia, nunc at ullamcorper condimentum, lacus enim gravida tellus, sit amet porta neque nibh at nisi. Nulla ornare arcu luctus viverra elementum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nulla non rutrum est. Vestibulum eget orci a dui tincidunt sollicitudin. Maecenas vestibulum posuere sagittis. Nunc ut nisl nulla.
            </span>
        </div>
    </section>

</main>

@endSection('content')
