<?php


namespace Exceptions\Validator;

use Exception;

class ValidationException extends Exception
{
    protected $code = 422;
    protected $message = 'Błąd walidacji!';

}