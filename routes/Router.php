<?php

namespace Routes;

class Router
{
    public static function get():array {
        return [
            '' => 'HomeController@index',
            'login' => 'LoginController@index',
            'register' => 'RegisterController@index',
            'logout' => 'LoginController@logout',
            'articles' => 'ArticleController@index',
            'articles/{id}' => 'ArticleController@article',
            'articles/create' => 'ArticleController@create',
            'articles/edit/{id}' => 'ArticleController@edit',
            'articles/my-articles' => 'ArticleController@myArticles',
            'admin/dashboard' => 'Admin\AdminController@index',
        ];
    }

    public static function post():array {
        return [
            'login' => 'LoginController@login',
            'register' => 'RegisterController@register',
            'articles/store' => 'ArticleController@store',
            'articles/update/{id}' => 'ArticleController@update',
            'articles/store/comment' => 'ArticleController@storeComment',
        ];
    }
}


