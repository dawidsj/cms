<?php

spl_autoload_register(function ($className) {
    if(file_exists($className.'.php')) {
        require_once $className.'.php';
    }
});

if(file_exists('env.php')) {
    $env = include_once('env.php');

    foreach ($env as $key => $value) {
        putenv("$key=$value");
    }
}
